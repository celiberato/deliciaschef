package br.com.celiberato.deliciaschef.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MessagesConstants {

	public static final String CLIENTE_INEXISTENTE = "Cliente não existe no banco de dados!";
	public static final String FUNCIONARIO_INEXISTENTE = "Funcionário não existe no banco de dados!";
	public static final String PEDIDO_INEXISTENTE = "Pedido não existe no banco de dados";
	public static final String ENDERECO_INEXISTENTE = "Endereço não existe no banco de dados!";

	public static final String PRODUTO_INEXISTENTE = "Produto não existe no banco de dados!";
	public static final String FORNECEDOR_INEXISTENTE = "Fornecedor não existe no banco de dados!";
	public static final String CATEGORIA_INEXISTENTE = "Categoria não existe no banco de dados!";
	
	public static final String CLIENTE_REQUERIDO = "Cliente é requerido!";
	public static final String FUNCIONARIO_REQUERIDO = "Funcionário é requerido!";
	public static final String PEDIDO_REQUERIDO = "Pedido é requerido!";
	public static final String ENDERECO_REQUERIDO = "Endereço é requerido!";

	public static final String PRODUTO_REQUERIDO = "Produto é requerido!";
	public static final String FORNECEDOR_REQUERIDO = "Fornecedor é requerido!";
	public static final String CATEGORIA_REQUERIDO = "Categoria é requerido!";
	
}
