package br.com.celiberato.deliciaschef.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.celiberato.deliciaschef.bootstrap.DatabaseLoader;
import br.com.celiberato.deliciaschef.exception.BusinessException;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping(value = "/start", produces = { MediaType.APPLICATION_JSON_VALUE})
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StartController extends BaseController {


	@Autowired
	DatabaseLoader databaseLoader;

	@GetMapping(value = "/loadAll")
	@ResponseStatus(HttpStatus.OK)
	public String load(@ApiIgnore Principal principal) throws BusinessException {
		databaseLoader.loadAll();
		return "OK";
	}
}
