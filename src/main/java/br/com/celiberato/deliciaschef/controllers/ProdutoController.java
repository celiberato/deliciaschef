package br.com.celiberato.deliciaschef.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.celiberato.deliciaschef.domain.Produto;
import br.com.celiberato.deliciaschef.domain.dto.IdentifierDTO;
import br.com.celiberato.deliciaschef.domain.dto.ProdutoDTO;
import br.com.celiberato.deliciaschef.domain.vo.FiltroProdutoVO;
import br.com.celiberato.deliciaschef.domain.vo.ProdutoVO;
import br.com.celiberato.deliciaschef.exception.BusinessException;
import br.com.celiberato.deliciaschef.services.ProdutoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping(value = "/produtos", produces = { MediaType.APPLICATION_JSON_VALUE})
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProdutoController extends BaseController {


	@Autowired
	ProdutoService produtoService;

	@PostMapping(consumes = "application/json", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public IdentifierDTO createProduto(@RequestBody @Valid final ProdutoVO vo, @ApiIgnore Principal principal) throws BusinessException {
		Produto produto = convertToEntity(vo, Produto.class);

		return new IdentifierDTO(produtoService.create(produto).getId());
	}

	@DeleteMapping(ID_PATH_VARIABLE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteProduto(@ApiParam(value = "Id do produto", required = true) @PathVariable(required = true) final Long id, @ApiIgnore Principal principal) throws BusinessException {
		produtoService.delete(id);
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiImplicitParams({ //
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Página a ser retornada (0..N)"), //
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Número de endereços por página") //
	})
	public Page<ProdutoDTO> findByFiltroProduto(FiltroProdutoVO filtro, @ApiIgnore Pageable pageable) throws BusinessException {
		return convertToDTO(produtoService.findByFiltro(filtro, pageable), ProdutoDTO.class);
	}

	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ProdutoDTO findByIdProduto(@ApiParam(value = "Identificador único", required = true) @PathVariable(required = true) final Long id) throws BusinessException {
		return convertToDTO(produtoService.findByIdProduto(id), ProdutoDTO.class);
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Atualizar Produto")
	public void updateProduto(@RequestBody @Valid final ProdutoVO vo, @PathVariable(required = true) final Long id) throws BusinessException {
		Produto produto = convertToEntity(vo, id, Produto.class);

		produtoService.update(produto);
	}

}
