package br.com.celiberato.deliciaschef.controllers;

import java.io.Serializable;

import br.com.celiberato.deliciaschef.domain.AbstractEntity;


public class BaseController extends AbstractController {


	protected <D, E extends AbstractEntity<I>, I extends Serializable> E convertToEntity(final D dto, final Class<E> entityClass) {
		return modelMapper.map(dto, entityClass);
	}

	@SuppressWarnings("null")
	protected <D, E extends AbstractEntity<I>, I extends Serializable> E convertToEntity(final D dto, final I id, final Class<E> entityClass) {
		E entity = modelMapper.map(dto, entityClass);
		entity.setId(id);

		return entity;
	}

}
