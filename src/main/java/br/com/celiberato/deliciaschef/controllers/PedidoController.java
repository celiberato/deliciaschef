package br.com.celiberato.deliciaschef.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.celiberato.deliciaschef.domain.Pedido;
import br.com.celiberato.deliciaschef.domain.dto.IdentifierDTO;
import br.com.celiberato.deliciaschef.domain.dto.PedidoDTO;
import br.com.celiberato.deliciaschef.domain.vo.FiltroPedidoVO;
import br.com.celiberato.deliciaschef.domain.vo.PedidoVO;
import br.com.celiberato.deliciaschef.exception.BusinessException;
import br.com.celiberato.deliciaschef.services.PedidoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import springfox.documentation.annotations.ApiIgnore;


@RestController(value = "/pedidos")
@RequestMapping(produces = { MediaType.APPLICATION_JSON_VALUE})
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PedidoController extends BaseController {


	@Autowired
	PedidoService pedidoService;

	@PostMapping(consumes = "application/json", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public IdentifierDTO createPedido(@RequestBody @Valid final PedidoVO vo, @ApiIgnore Principal principal) throws BusinessException {
		Pedido pedido = convertToEntity(vo, Pedido.class);

		return new IdentifierDTO(pedidoService.create(pedido).getId());
	}

	@DeleteMapping(ID_PATH_VARIABLE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletePedido(@ApiParam(value = "Id do pedido", required = true) @PathVariable(required = true) final Long id, @ApiIgnore Principal principal) throws BusinessException {
		pedidoService.delete(id);
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiImplicitParams({ //
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Página a ser retornada (0..N)"), //
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Número de endereços por página") //
	})
	public Page<PedidoDTO> findByFiltroPedido(FiltroPedidoVO filtro, @ApiIgnore Pageable pageable) throws BusinessException {
		return convertToDTO(pedidoService.findByFiltro(filtro, pageable), PedidoDTO.class);
	}

	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public PedidoDTO findByIdPedido(@ApiParam(value = "Identificador único", required = true) @PathVariable(required = true) final Long id) throws BusinessException {
		return convertToDTO(pedidoService.findByIdFecthAll(id), PedidoDTO.class);
	}

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation(value = "Atualizar Pedido")
	public void updatePedido(@RequestBody @Valid final PedidoVO vo, @PathVariable(required = true) final Long id) throws BusinessException {
		Pedido pedido = convertToEntity(vo, id, Pedido.class);

		pedidoService.update(pedido);
	}

}
