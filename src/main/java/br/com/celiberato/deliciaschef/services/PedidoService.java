package br.com.celiberato.deliciaschef.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.celiberato.deliciaschef.constants.MessagesConstants;
import br.com.celiberato.deliciaschef.domain.ItemPedido;
import br.com.celiberato.deliciaschef.domain.Pedido;
import br.com.celiberato.deliciaschef.domain.vo.FiltroPedidoVO;
import br.com.celiberato.deliciaschef.exception.BusinessException;
import br.com.celiberato.deliciaschef.exception.RecordNotFoundException;
import br.com.celiberato.deliciaschef.repositories.ClienteRepository;
import br.com.celiberato.deliciaschef.repositories.CupomRepository;
import br.com.celiberato.deliciaschef.repositories.EnderecoRepository;
import br.com.celiberato.deliciaschef.repositories.FuncionarioRepository;
import br.com.celiberato.deliciaschef.repositories.PedidoRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PedidoService {

    @Autowired
	private PedidoRepository pedidoRepository;

    @Autowired
	private ClienteRepository clienteRepository;

    @Autowired
	private FuncionarioRepository funcionarioRepository;

    @Autowired
	private EnderecoRepository enderecoRepository;

	public Pedido create(Pedido pedido) throws BusinessException {

    	validate(pedido);
    	
    	calculate(pedido);

		return pedidoRepository.save(pedido);
	}

	public void delete(final Long id) throws BusinessException{
		try {
			pedidoRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new BusinessException(MessagesConstants.PEDIDO_INEXISTENTE, HttpStatus.NOT_FOUND);
		}
	}

	public Page<Pedido> findByFiltro(FiltroPedidoVO filtro, Pageable pageable) {
		return pedidoRepository.findByFiltro(filtro, pageable);
	}

	public Optional<Pedido> findByIdFecthAll(final Long id) throws RecordNotFoundException{
		return pedidoRepository.findByIdFecthAll(id);
	}


	public Pedido update(final Pedido pedido) throws BusinessException {

    	validate(pedido);

    	calculate(pedido);

    	if (!pedidoRepository.existsById(pedido.getId())) {
			throw new RecordNotFoundException();
		}

		return pedidoRepository.save(pedido);
	}

	public void calculate(Pedido pedido) throws BusinessException{
		Double valorFinal = 0d;
		
		if(pedido.getItens()!=null) {
			for(ItemPedido item: pedido.getItens()) {
				valorFinal += (item.getPrecoUnitario() * item.getQuantidade()) - item.getDesconto();
			}
		}
		
		if(pedido.getCupom()!=null) {
			valorFinal -= pedido.getCupom().getValorDesconto();
		}
		
		pedido.setValorFinal(Math.max(0d, valorFinal));
	}

	
	public void validate(Pedido pedido) throws BusinessException{
		
		if(!ObjectUtils.isEmpty(pedido.getId()) && !pedidoRepository.existsById(pedido.getId())){
			throw new BusinessException(MessagesConstants.PEDIDO_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
		
		if(ObjectUtils.isEmpty(pedido.getCliente()) || ObjectUtils.isEmpty(pedido.getCliente().getId())){
			throw new BusinessException(MessagesConstants.CLIENTE_REQUERIDO, HttpStatus.BAD_REQUEST);
		} else if(!clienteRepository.existsById(pedido.getCliente().getId())) {
			throw new BusinessException(MessagesConstants.CLIENTE_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}

		if(ObjectUtils.isEmpty(pedido.getFuncionario()) || ObjectUtils.isEmpty(pedido.getFuncionario().getId())){
			throw new BusinessException(MessagesConstants.FUNCIONARIO_REQUERIDO, HttpStatus.BAD_REQUEST);
		} else if(!funcionarioRepository.existsById(pedido.getFuncionario().getId())) {
			throw new BusinessException(MessagesConstants.FUNCIONARIO_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
		
		if(ObjectUtils.isEmpty(pedido.getEndereco()) || ObjectUtils.isEmpty(pedido.getEndereco().getId())){
			throw new BusinessException(MessagesConstants.ENDERECO_REQUERIDO, HttpStatus.BAD_REQUEST);
		} else if(!enderecoRepository.existsById(pedido.getEndereco().getId())) {
			throw new BusinessException(MessagesConstants.ENDERECO_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
	}
}
