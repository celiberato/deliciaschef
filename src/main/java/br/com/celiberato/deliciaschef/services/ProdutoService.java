package br.com.celiberato.deliciaschef.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.celiberato.deliciaschef.constants.MessagesConstants;
import br.com.celiberato.deliciaschef.domain.Produto;
import br.com.celiberato.deliciaschef.domain.vo.FiltroProdutoVO;
import br.com.celiberato.deliciaschef.exception.BusinessException;
import br.com.celiberato.deliciaschef.exception.RecordNotFoundException;
import br.com.celiberato.deliciaschef.repositories.CategoriaRepository;
import br.com.celiberato.deliciaschef.repositories.FornecedorRepository;
import br.com.celiberato.deliciaschef.repositories.ProdutoRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProdutoService {

    @Autowired
	private ProdutoRepository produtoRepository;
    
    @Autowired
	private FornecedorRepository fornecedorRepository;

    @Autowired
	private CategoriaRepository categoriaRepository;


	public Produto create(Produto produto) throws BusinessException {

    	validate(produto);

		return produtoRepository.save(produto);
	}

	public void delete(final Long id) throws BusinessException{
		try {
			produtoRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new BusinessException(MessagesConstants.PRODUTO_INEXISTENTE, HttpStatus.NOT_FOUND);
		}
	}

	public Page<Produto> findByFiltro(FiltroProdutoVO filtro, Pageable pageable) {
		return produtoRepository.findByFiltro(filtro, pageable);
	}

	public Optional<Produto> findByIdProduto(final Long id) throws RecordNotFoundException{
		return produtoRepository.findByIdFecthAll(id);
	}


	public Produto update(final Produto produto) throws BusinessException {

    	validate(produto);

    	if (!produtoRepository.existsById(produto.getId())) {
			throw new RecordNotFoundException();
		}

		return produtoRepository.save(produto);
	}
	
	public void validate(Produto produto) throws BusinessException{
		
		if(!ObjectUtils.isEmpty(produto.getId()) && !produtoRepository.existsById(produto.getId())){
			throw new BusinessException(MessagesConstants.PRODUTO_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
		
		if(ObjectUtils.isEmpty(produto.getFornecedor()) || ObjectUtils.isEmpty(produto.getFornecedor().getId())){
			throw new BusinessException(MessagesConstants.FORNECEDOR_REQUERIDO, HttpStatus.BAD_REQUEST);
		} else if(!fornecedorRepository.existsById(produto.getFornecedor().getId())) {
			throw new BusinessException(MessagesConstants.FORNECEDOR_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}

		if(ObjectUtils.isEmpty(produto.getCategoria()) || ObjectUtils.isEmpty(produto.getCategoria	().getId())){
			throw new BusinessException(MessagesConstants.CATEGORIA_REQUERIDO, HttpStatus.BAD_REQUEST);
		} else if(!categoriaRepository.existsById(produto.getCategoria().getId())) {
			throw new BusinessException(MessagesConstants. CATEGORIA_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
	}
}
