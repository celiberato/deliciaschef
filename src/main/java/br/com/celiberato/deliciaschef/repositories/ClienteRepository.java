package br.com.celiberato.deliciaschef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiberato.deliciaschef.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

}
