package br.com.celiberato.deliciaschef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiberato.deliciaschef.domain.Uf;

public interface UfRepository extends JpaRepository<Uf, Short> {

}
