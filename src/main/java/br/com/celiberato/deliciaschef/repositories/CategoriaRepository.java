package br.com.celiberato.deliciaschef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiberato.deliciaschef.domain.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {

}
