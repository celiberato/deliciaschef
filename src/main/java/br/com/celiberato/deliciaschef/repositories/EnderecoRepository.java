package br.com.celiberato.deliciaschef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiberato.deliciaschef.domain.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Long> {

}
