package br.com.celiberato.deliciaschef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiberato.deliciaschef.domain.Fornecedor;

public interface FornecedorRepository extends JpaRepository<Fornecedor, Long> {

}
