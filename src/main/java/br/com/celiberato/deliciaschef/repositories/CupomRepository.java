package br.com.celiberato.deliciaschef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiberato.deliciaschef.domain.Cupom;

public interface CupomRepository extends JpaRepository<Cupom, Long> {

	
}
