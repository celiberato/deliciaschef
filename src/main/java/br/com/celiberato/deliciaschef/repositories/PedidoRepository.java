package br.com.celiberato.deliciaschef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.celiberato.deliciaschef.domain.Pedido;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long>, PedidoRepositoryCustom {
}
