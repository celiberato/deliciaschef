package br.com.celiberato.deliciaschef.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.celiberato.deliciaschef.domain.Pedido;
import br.com.celiberato.deliciaschef.domain.vo.FiltroPedidoVO;

public interface PedidoRepositoryCustom {

	Optional<Pedido> findByIdFecthAll(Long id);
	
	Page<Pedido> findByFiltro(FiltroPedidoVO filtro, Pageable pageable);
}
