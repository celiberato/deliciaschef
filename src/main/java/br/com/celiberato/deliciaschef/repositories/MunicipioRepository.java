package br.com.celiberato.deliciaschef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiberato.deliciaschef.domain.Municipio;

public interface MunicipioRepository extends JpaRepository<Municipio, Integer> {

}
