package br.com.celiberato.deliciaschef.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.celiberato.deliciaschef.domain.ItemPedido;

public interface ItemPedidoRepository extends JpaRepository<ItemPedido, Integer> {

}
