package br.com.celiberato.deliciaschef.repositories;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import br.com.celiberato.deliciaschef.domain.Pedido;
import br.com.celiberato.deliciaschef.domain.vo.FiltroPedidoVO;
import br.com.celiberato.deliciaschef.domain.Cliente_;
import br.com.celiberato.deliciaschef.domain.Endereco_;
import br.com.celiberato.deliciaschef.domain.Funcionario_;
import br.com.celiberato.deliciaschef.domain.ItemPedido_;
import br.com.celiberato.deliciaschef.domain.Municipio_;
import br.com.celiberato.deliciaschef.domain.Pedido_;
import br.com.celiberato.deliciaschef.domain.Produto_;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@FieldDefaults(level = AccessLevel.PROTECTED)
public class PedidoRepositoryCustomImpl implements PedidoRepositoryCustom {

	@Autowired
	EntityManager entityManager;
	
	private Long countResult(final FiltroPedidoVO filtro) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cBuilder.createQuery(Long.class);
		Root<Pedido> root = cq.from(Pedido.class);

		root.join(Pedido_.endereco, JoinType.LEFT).join(Endereco_.municipio, JoinType.LEFT).join( Municipio_.uf, JoinType.LEFT);
		root.join(Pedido_.cliente, JoinType.LEFT);
		root.join(Pedido_.funcionario, JoinType.LEFT);
		root.join(Pedido_.endereco, JoinType.LEFT);
		root.join(Pedido_.cupom, JoinType.LEFT);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Long> countSelect = cq.select(cBuilder.count(root.get(Pedido_.id)));
		countSelect.where(predicates.toArray(new Predicate[predicates.size()]));

		return entityManager.createQuery(countSelect).getSingleResult();

	}

	private List<Predicate> createPredicateFindByFilter(final FiltroPedidoVO filtro, CriteriaBuilder cb, Root<Pedido> root) {
		List<Predicate> predicates = new ArrayList<>();

		if (Objects.nonNull(filtro.getIdPedido())) {
			predicates.add(cb.equal(root.get(Pedido_.id), filtro.getIdPedido()));
		}

		if (Objects.nonNull(filtro.getIdCliente())) {
			predicates.add(cb.equal(root.get(Pedido_.cliente).get(Cliente_.id), filtro.getIdCliente()));
		}

		if (Objects.nonNull(filtro.getIdFuncionario())) {
			predicates.add(cb.equal(root.get(Pedido_.funcionario).get(Funcionario_.id), filtro.getIdFuncionario()));
		}

		if (Objects.nonNull(filtro.getLogradouro())) {
			predicates.add(cb.like(cb.lower(root.get(Pedido_.endereco).get(Endereco_.logradouro)), '%' + filtro.getLogradouro().toLowerCase() + '%'));
		}

		return predicates;
	}

	@Override
	public Page<Pedido> findByFiltro(FiltroPedidoVO filtro, Pageable pageable) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Pedido> cQuery = cBuilder.createQuery(Pedido.class);

		Root<Pedido> root = cQuery.from(Pedido.class);
		root.fetch(Pedido_.endereco, JoinType.LEFT).fetch(Endereco_.municipio, JoinType.LEFT).fetch(Municipio_.uf, JoinType.LEFT);
		root.fetch(Pedido_.cliente, JoinType.LEFT);
		root.fetch(Pedido_.funcionario, JoinType.LEFT);
		root.fetch(Pedido_.endereco, JoinType.LEFT);
		root.fetch(Pedido_.itens, JoinType.LEFT).fetch(ItemPedido_.produto, JoinType.LEFT).fetch(Produto_.categoria, JoinType.LEFT);
		root.fetch(Pedido_.cupom, JoinType.LEFT);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);

		CriteriaQuery<Pedido> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		cQuery.orderBy(cBuilder.desc(root.get(Pedido_.dataPedido)));

		Long count = countResult(filtro);
		if (count == 0) {
			return new PageImpl<>(Collections.emptyList(), pageable, count);
		}

		TypedQuery<Pedido> query = entityManager.createQuery(select);
		query.setFirstResult((int) pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		List<Pedido> page = query.getResultList();
		for(Pedido pedido: page) {
			pedido.setItens(new ArrayList<>(pedido.getItens()));
		}
		
		return new PageImpl<>(page, pageable, count);
	}

	@Override
	public Optional<Pedido> findByIdFecthAll(Long id) {
		CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Pedido> cQuery = cBuilder.createQuery(Pedido.class);

		Root<Pedido> root = cQuery.from(Pedido.class);
		root.fetch(Pedido_.endereco, JoinType.LEFT).fetch(Endereco_.municipio, JoinType.LEFT).fetch(Municipio_.uf, JoinType.LEFT);
		root.fetch(Pedido_.cliente, JoinType.LEFT);
		root.fetch(Pedido_.funcionario, JoinType.LEFT);
		root.fetch(Pedido_.endereco, JoinType.LEFT);
		root.fetch(Pedido_.itens, JoinType.LEFT).fetch(ItemPedido_.produto, JoinType.LEFT).fetch(Produto_.categoria, JoinType.LEFT);
		root.fetch(Pedido_.cupom, JoinType.LEFT);

		FiltroPedidoVO filtro = new FiltroPedidoVO(id);

		List<Predicate> predicates = createPredicateFindByFilter(filtro, cBuilder, root);
		CriteriaQuery<Pedido> select = cQuery.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<Pedido> query = entityManager.createQuery(select);

		try {
			Pedido pedido = query.getSingleResult();
			pedido.setItens(new ArrayList<>(pedido.getItens()));

			return Optional.ofNullable(pedido);

		} catch (Exception e) {
			log.debug(e.getMessage(), e);
		}

		return Optional.empty();
	}

}
