package br.com.celiberato.deliciaschef.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.celiberato.deliciaschef.domain.Produto;
import br.com.celiberato.deliciaschef.domain.vo.FiltroProdutoVO;

public interface ProdutoRepositoryCustom {

	Optional<Produto> findByIdFecthAll(Long id);
	
	Page<Produto> findByFiltro(FiltroProdutoVO filtro, Pageable pageable);
}
