package br.com.celiberato.deliciaschef.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.celiberato.deliciaschef.constants.SchemaConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
@Entity
public class Cupom extends AbstractEntity<Long> {

	static final long serialVersionUID = 1l;

	@Column(name = "CODIGO_CUPOM")
	private String codigoCupom;
	
	@Column(name = "VALOR_DESCONTO")
	private Double valorDesconto;

}