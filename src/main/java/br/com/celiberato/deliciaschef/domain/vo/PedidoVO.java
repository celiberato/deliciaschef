package br.com.celiberato.deliciaschef.domain.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import br.com.celiberato.deliciaschef.domain.ItemPedido;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class PedidoVO {

	static final long serialVersionUID = 1l;

	Long id;
	List<ItemPedido> itens;
	ClienteVO cliente;
	FuncionarioVO funcionario;
	LocalDateTime dataPedido;
	LocalDateTime dataEntrega;
	LocalDateTime dataEnvio;
	Double frete;
	String nomeDestinatario;
	EnderecoVO endereco;
	
}