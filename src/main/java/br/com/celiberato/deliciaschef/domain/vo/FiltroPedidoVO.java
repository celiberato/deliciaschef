package br.com.celiberato.deliciaschef.domain.vo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FiltroPedidoVO {

	Long idPedido;

	Long idCliente;

	Long idFuncionario;

	String logradouro;

	public FiltroPedidoVO(Long idPedido) {
		this.idPedido = idPedido;	
	}
}
