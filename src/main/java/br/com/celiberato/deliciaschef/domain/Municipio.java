package br.com.celiberato.deliciaschef.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.celiberato.deliciaschef.constants.SchemaConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Municipio extends AbstractEntity<Integer> {

	static final long serialVersionUID = 1L;

	@Column(name = "NOME_MUNICIPIO")
	String nome;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_UF", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_MUNICIPIO_UF"))
	Uf uf;

}
