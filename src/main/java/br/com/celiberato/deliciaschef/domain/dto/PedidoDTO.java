package br.com.celiberato.deliciaschef.domain.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.celiberato.deliciaschef.domain.Cupom;
import br.com.celiberato.deliciaschef.domain.ItemPedido;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonInclude(Include.NON_NULL)
public class PedidoDTO {

	Long id;
	List<ItemPedidoDTO> itens = new ArrayList<>();
	ClienteDTO cliente;
	FuncionarioDTO funcionario;
	LocalDateTime dataPedido;
	LocalDate dataEntrega;
	LocalDate dataEnvio;
	Double frete;
	String nomeDestinatario;
	
	EnderecoDTO endereco;

	CupomDTO cupom;
	Double valorFinal;

}
