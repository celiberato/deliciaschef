package br.com.celiberato.deliciaschef.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.celiberato.deliciaschef.constants.SchemaConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
@Entity
public class ItemPedido extends AbstractEntity<Long> {

	static final long serialVersionUID = 1l;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PRODUTO", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_ITEM_PEDIDO_PRODUTO"))
	private Produto produto;

	@Column(name = "PRECO_UNITARIO")
	private Double precoUnitario;

	@Column(name = "QUANTIDADE")
	private Integer quantidade;

	@Column(name = "DESCONTO")
	private Double desconto;

	
}