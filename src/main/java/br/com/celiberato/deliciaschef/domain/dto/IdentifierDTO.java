package br.com.celiberato.deliciaschef.domain.dto;

import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class IdentifierDTO {

	Long id;
	String timestamp;
	String status;
	String error;
	String message;
	String path;

	public IdentifierDTO(Long id) {
		this.id = id;
	}
}
