package br.com.celiberato.deliciaschef.domain.vo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EnderecoVO {

	static final long serialVersionUID = 1L;

	Long id;
	
	String nomePais;
	MunicipioVO municipio;
	String logradouro;
	String numero;
	String complemento;
	String cep;
	String bairro;

}
