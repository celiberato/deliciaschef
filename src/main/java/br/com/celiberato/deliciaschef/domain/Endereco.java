package br.com.celiberato.deliciaschef.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.celiberato.deliciaschef.constants.SchemaConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Endereco extends AbstractEntity<Long> {

	static final long serialVersionUID = 1L;

	@Column(name = "NOME_PAIS")
	String nomePais;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_MUNICIPIO", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_ENDERECO_MUNICIPIO"))
	Municipio municipio;

	@Column(name = "LOGRADOURO")
	String logradouro;

	@Column(name = "NUMERO_LOGRADOURO")
	String numero;

	@Column(name = "COMPLEMENTO")
	String complemento;

	@Column(name = "CEP", columnDefinition = "CHAR(8)")
	String cep;

	@Column(name = "BAIRRO")
	String bairro;

}
