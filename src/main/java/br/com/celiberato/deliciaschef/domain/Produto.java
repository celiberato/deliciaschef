package br.com.celiberato.deliciaschef.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.celiberato.deliciaschef.constants.SchemaConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
@Entity
public class Produto extends AbstractEntity<Long> {

	static final long serialVersionUID = 1l;

	@Column(name = "NOME_PRODUTO")
	private String nomeProduto;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_FORNECEDOR", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PRODUTO_FORNECEDOR"))	
	private Fornecedor fornecedor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CATEGORIA", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PRODUTO_CATEGORIA"))	
	private Categoria categoria;

	@Column(name = "QTDE_POR_UNIDADE")
	private Integer quantidadePorUnidade;

	@Column(name = "PRECO_UNITARIO")
	private Double precoUnitario;
	
	@Column(name = "QTDE_ESTOQUE")
	private Integer quantidadeEstoque;

	@Column(name = "FIGURA")
	private String figura;

}