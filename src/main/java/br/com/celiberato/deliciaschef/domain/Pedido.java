package br.com.celiberato.deliciaschef.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.celiberato.deliciaschef.constants.SchemaConstants;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
@Entity	
public class Pedido extends AbstractEntity<Long> {

	static final long serialVersionUID = 1l;

	private String title;
	 
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true) 
    private List<ItemPedido> itens = new ArrayList<>();
 
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PEDIDO_CLIENTE"))
	Cliente cliente;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_FUNCIONARIO", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PEDIDO_FUNCIONARIO"))
	Funcionario funcionario;

	@Column(name = "DATA_PEDIDO")
	LocalDateTime dataPedido;

	@Column(name = "DATA_ENTREGA")
	LocalDateTime dataEntrega;

	@Column(name = "DATA_ENVIO")
	LocalDateTime dataEnvio;

	@Column(name = "FRETE")
	Double frete;

	@Column(name = " NOME_DESTINATARIO")
	String nomeDestinatario;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ENDERECO", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PEDIDO_ENDERECO"))
	Endereco endereco;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CUPOM", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_PEDIDO_CUPOM"))
	Cupom cupom;

	@Column(name = "VALOR_FINAL")
	Double valorFinal;

	
}	