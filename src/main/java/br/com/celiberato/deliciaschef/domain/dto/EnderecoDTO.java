package br.com.celiberato.deliciaschef.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EnderecoDTO {

	static final long serialVersionUID = 1L;

	Long id;
	
	String nomePais;
	
	@JsonIgnore
	MunicipioDTO municipio;
	
	String logradouro;
	String numero;
	String complemento;
	String cep;
	String bairro;

}
