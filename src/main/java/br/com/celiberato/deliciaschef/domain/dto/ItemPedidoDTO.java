package br.com.celiberato.deliciaschef.domain.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class ItemPedidoDTO {

	static final long serialVersionUID = 1l;

	private Long id;
	private ProdutoDTO produto;
	private Double precoUnitario;
	private Integer quantidade;
	private Double desconto;

}