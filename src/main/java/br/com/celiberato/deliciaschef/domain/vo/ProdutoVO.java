package br.com.celiberato.deliciaschef.domain.vo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder 
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@FieldDefaults(level = AccessLevel.PROTECTED)
public class ProdutoVO {

	static final long serialVersionUID = 1l;

	Long id;
	String nomeProduto;
	FornecedorVO fornecedor;
	CategoriaVO categoria;
	Integer quantidadePorUnidade;
	Double precoUnitario;
	Integer quantidadeEstoque;
	String figura;

}