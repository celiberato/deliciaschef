package br.com.celiberato.deliciaschef.tests.repository;

import br.com.celiberato.deliciaschef.tests.container.DefaultPostgresContainer;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class AbstractRepositoryIT {

	static {
		if (DefaultPostgresContainer.isEnabled()) {
			DefaultPostgresContainer.getInstance().start();
		}
	}

}
