package br.com.celiberato.deliciaschef.bootstrap;

import static org.junit.Assert.assertNotNull;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.celiberato.deliciaschef.domain.Categoria;
import br.com.celiberato.deliciaschef.domain.Cliente;
import br.com.celiberato.deliciaschef.domain.Cupom;
import br.com.celiberato.deliciaschef.domain.Endereco;
import br.com.celiberato.deliciaschef.domain.Fornecedor;
import br.com.celiberato.deliciaschef.domain.Funcionario;
import br.com.celiberato.deliciaschef.domain.ItemPedido;
import br.com.celiberato.deliciaschef.domain.Municipio;
import br.com.celiberato.deliciaschef.domain.Pedido;
import br.com.celiberato.deliciaschef.domain.Produto;
import br.com.celiberato.deliciaschef.domain.Uf;
import br.com.celiberato.deliciaschef.exception.BusinessException;
import br.com.celiberato.deliciaschef.repositories.CategoriaRepository;
import br.com.celiberato.deliciaschef.repositories.ClienteRepository;
import br.com.celiberato.deliciaschef.repositories.CupomRepository;
import br.com.celiberato.deliciaschef.repositories.EnderecoRepository;
import br.com.celiberato.deliciaschef.repositories.FornecedorRepository;
import br.com.celiberato.deliciaschef.repositories.FuncionarioRepository;
import br.com.celiberato.deliciaschef.repositories.ItemPedidoRepository;
import br.com.celiberato.deliciaschef.repositories.MunicipioRepository;
import br.com.celiberato.deliciaschef.repositories.PedidoRepository;
import br.com.celiberato.deliciaschef.repositories.ProdutoRepository;
import br.com.celiberato.deliciaschef.repositories.UfRepository;
import br.com.celiberato.deliciaschef.services.PedidoService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@FieldDefaults(level = AccessLevel.PUBLIC)
public class DatabaseLoader implements Serializable {

    private Logger log = LogManager.getLogger(DatabaseLoader.class);

	@Autowired
	CupomRepository cupomRepository;

	@Autowired
	EnderecoRepository enderecoRepository;

	@Autowired
	MunicipioRepository municipioRepository;

	@Autowired
	UfRepository ufRepository;

	@Autowired
	PedidoRepository pedidoRepository;

	PedidoService pedidoService = new PedidoService();

	@Autowired
	ClienteRepository clienteRepository;

	@Autowired
	FuncionarioRepository funcionarioRepository;

	@Autowired
	ProdutoRepository produtoRepository;

	@Autowired
	CategoriaRepository categoriaRepository;

	@Autowired
	FornecedorRepository fornecedorRepository;

	@Autowired
	ItemPedidoRepository itemPedidoRepository;

	public void deleteAll() {
		pedidoService.setPedidoRepository(pedidoRepository);
		pedidoService.setClienteRepository(clienteRepository);
		pedidoService.setFuncionarioRepository(funcionarioRepository);
		pedidoService.setEnderecoRepository(enderecoRepository);
		
		pedidoRepository.deleteAllInBatch();
		itemPedidoRepository.deleteAllInBatch();
		produtoRepository.deleteAllInBatch();
		categoriaRepository.deleteAllInBatch();
		clienteRepository.deleteAllInBatch();
		fornecedorRepository.deleteAllInBatch();
		funcionarioRepository.deleteAllInBatch();
		enderecoRepository.deleteAllInBatch();
		municipioRepository.deleteAllInBatch();
		ufRepository.deleteAllInBatch();
	}

	public void loadAll() {
		deleteAll();
		
		List<Produto> listaProdutos = createProdutos();
		List<ItemPedido> listaItens = createItensPedidos();
		List<Pedido> listaPedidos = createPedidos();
	}
		
	public Pedido builderPedido() {
		return Pedido.builder()//
				.cliente(createCliente())//
				.funcionario(createFuncionario())//
				.endereco(createEndereco())//
				.dataEnvio(LocalDateTime.now())//
				.dataPedido(LocalDateTime.now())//
				.dataEntrega(LocalDateTime.now())//
				.frete(100.00d)//
				.nomeDestinatario("xxxxxxxxx")//
				.build();
	}

	public Cliente builderCliente() {
		return Cliente.builder()//
				.endereco(createEndereco())//
				.telefone("11-988888888")//
				.build();
	}

	public Funcionario builderFuncionario() {
		return Funcionario.builder()//
				.nome("José Silva")//
				.email("teste@gmail.com")//
				.build();
	}

	public Categoria builderCategoria() {
		return Categoria.builder()//
				.nomeCategoria("ESPORTE")//
				.descricao("Categoria de esportes")//
				.build();
	}

	public Cupom builderCupom() {
		return Cupom.builder()//
				.codigoCupom("ESPECIAL")//
				.valorDesconto(15d)//
				.build();
	}

	public Municipio builderMunicipio() {
		return Municipio.builder()//
				.uf(createUf())//
				.nome("São Paulo")//
				.build();
	}

	public Uf builderUf() {
		return Uf.builder()//
				.sigla("SP")//
				.nome("São Paulo")//
				.build();
	}

	public Endereco builderEndereco() {
		return Endereco.builder()//
				.municipio(createMunicipio())//
				.nomePais("Brasil")//
				.logradouro("Av. Paulista")//
				.numero("1100")//
				.complemento("-")//
				.cep("02187051")//
				.bairro("Jardins")//
				.build();
	}

	public Produto builderProduto(String nomeProduto, String figura) {
		return Produto.builder()//
				.categoria(createCategoria())//
				.fornecedor(createFornecedor())//
				.nomeProduto(nomeProduto)//
				.quantidadePorUnidade(12)//
				.precoUnitario(120d)//
				.figura(figura)//
				.build();
	}

	public ItemPedido builderItemPedido(String nomeProduto, String figura) {
		return ItemPedido.builder()//
				.produto(createProduto(nomeProduto, figura))//
				.desconto(15.0d)
				.precoUnitario(18d)
				.quantidade(12)//
				.build();
	}

	public Fornecedor builderFornecedor() {
		return Fornecedor.builder()//
				.endereco(createEndereco())//
				.nomeEmpresa("xpto")//
				.nomeContato("Mario")//
				.build();
	}

	public List<Pedido> createPedidos() {
		List<Pedido> pedidos = new ArrayList<>();
		pedidos.add(createPedido());
		pedidos.add(createPedido());
		pedidos.add(createPedido());

		pedidos.forEach((pedido) -> {
			List<ItemPedido> listaItens = createItensPedidos();
			pedido.setItens(listaItens);
			pedido.setCupom(createCupom());
			
			try {
				pedidoService.update(pedido);
			} catch (BusinessException e) {
				System.out.println(e.getMessage());
			}

			assertNotNull(pedido.getId());
		});

		return pedidos;
	}

	
	public Pedido createPedido() {
		Pedido pedido = builderPedido();
		
		pedidoRepository.save(pedido);
		return pedido;
	}

	public Funcionario createFuncionario() {
		Funcionario funcionario = builderFuncionario();
		
		funcionarioRepository.save(funcionario);
		return funcionario;
	}

	public Fornecedor createFornecedor() {
		Fornecedor fornecedor = builderFornecedor();
		
		fornecedorRepository.save(fornecedor);
		return fornecedor;
	}

	public Categoria createCategoria() {
		Categoria categoria = builderCategoria();
		
		categoriaRepository.save(categoria);
		return categoria;
	}

	public Cupom createCupom() {
		Cupom cupom = builderCupom();
		
		cupomRepository.save(cupom);
		return cupom;
	}

	public List<Produto> createProdutos() {
		List<Produto> lista = new ArrayList<>();
		
		lista.add(createProduto("Banana", "figura01.jpg"));
		lista.add(createProduto("Maça", "figura02.jpg"));
		lista.add(createProduto("Cenoura", "figura03.jpg"));
		lista.add(createProduto("Alface", "figura04.jpg"));
		lista.add(createProduto("Manga", "figura05.jpg"));
		
		produtoRepository.saveAll(lista);
		return lista;
	}

	public List<ItemPedido> createItensPedidos() {
		List<ItemPedido> lista = new ArrayList<>();
		
		lista.add(builderItemPedido("Maracujá", "figura01.jpg"));
		lista.add(builderItemPedido("Tomate", "figura02.jpg"));
		lista.add(builderItemPedido("Couve", "figura03.jpg"));
		lista.add(builderItemPedido("Agrião", "figura04.jpg"));
		lista.add(builderItemPedido("Rúcula", "figura05.jpg"));
		
		itemPedidoRepository.saveAll(lista);
		return lista;
	}

	public Produto createProduto(String nomeProduto, String figura) {
		Produto produto = builderProduto(nomeProduto, figura);
		
		produtoRepository.save(produto);
		return produto;
	}

	public ItemPedido createItemPedido(String nomeProduto, String figura) {
		ItemPedido item = builderItemPedido(nomeProduto, figura);
		
		return item;
	}

	public Cliente createCliente() {
		Cliente cliente = builderCliente();
		
		clienteRepository.save(cliente);
		return cliente;
	}

	
	public Endereco createEndereco() {
		Endereco endereco = builderEndereco();
		
		enderecoRepository.save(endereco);
		return endereco;
	}

	
	public Municipio createMunicipio() {
		Municipio municipio = builderMunicipio();
		
		municipioRepository.save(municipio);
		return municipio;
	}

	public Uf createUf() {
		Uf uf = builderUf();
		
		ufRepository.save(uf);
		return uf;
	}
}
