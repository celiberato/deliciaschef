package br.com.celiberato.deliciaschef.repositories;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.celiberato.deliciaschef.bootstrap.DatabaseLoader;
import br.com.celiberato.deliciaschef.configuration.RepositoryConfiguration;
import br.com.celiberato.deliciaschef.constants.MessagesConstants;
import br.com.celiberato.deliciaschef.domain.Pedido;
import br.com.celiberato.deliciaschef.domain.dto.PedidoDTO;
import br.com.celiberato.deliciaschef.domain.vo.FiltroPedidoVO;
import br.com.celiberato.deliciaschef.domain.vo.PedidoVO;
import br.com.celiberato.deliciaschef.exception.BusinessException;
import br.com.celiberato.deliciaschef.services.PedidoService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {RepositoryConfiguration.class})
public class PedidoRepositoryTest extends BaseControllerIT {

	public static final String HTTP = "http";
	public static final String HOST = "localhost";
	public static final Integer PORT = 3025;
	public static final String BASE_URL = "/pedidos/";

    private ModelMapper modelMapper =  new ModelMapper();

	static final String RESOURCE_BASE_URL = "";

	TestRestTemplate restTemplate =  new TestRestTemplate(new RestTemplateBuilder().rootUri("http://localhost:3024/pedidos/"));

	PedidoService pedidoService = new PedidoService();

	DatabaseLoader databaseLoader = new DatabaseLoader();

	@Autowired
	CupomRepository cupomRepository;

	@Autowired
	EnderecoRepository enderecoRepository;

	@Autowired
	MunicipioRepository municipioRepository;

	@Autowired
	UfRepository ufRepository;

	@Autowired
	PedidoRepository pedidoRepository;

	@Autowired
	ClienteRepository clienteRepository;

	@Autowired
	FuncionarioRepository funcionarioRepository;

	@Autowired
	ProdutoRepository produtoRepository;

	@Autowired
	CategoriaRepository categoriaRepository;

	@Autowired
	FornecedorRepository fornecedorRepository;

	@Autowired
	ItemPedidoRepository itemPedidoRepository;


	@Before
	public void setUp() {
		pedidoService.setPedidoRepository(pedidoRepository);
		pedidoService.setClienteRepository(clienteRepository);
		pedidoService.setFuncionarioRepository(funcionarioRepository);
		pedidoService.setEnderecoRepository(enderecoRepository);

		databaseLoader.setPedidoService(pedidoService);
		databaseLoader.setPedidoRepository(pedidoRepository);
		databaseLoader.setClienteRepository(clienteRepository);
		databaseLoader.setFuncionarioRepository(funcionarioRepository);
		databaseLoader.setEnderecoRepository(enderecoRepository);	
		databaseLoader.setMunicipioRepository(municipioRepository);
		databaseLoader.setUfRepository(ufRepository);
		databaseLoader.setProdutoRepository(produtoRepository);
		databaseLoader.setCategoriaRepository(categoriaRepository);
		databaseLoader.setFornecedorRepository(fornecedorRepository);
		databaseLoader.setItemPedidoRepository(itemPedidoRepository);
		databaseLoader.setCupomRepository(cupomRepository);
		databaseLoader.deleteAll();
	}
	
	
	@Test	
	public void testDeleteOK() throws BusinessException, URISyntaxException {
		PedidoVO pedido = convertToVO(databaseLoader.createPedido(), PedidoVO.class);
		assertTrue(pedidoRepository.existsById(pedido.getId()));
		
		pedidoRepository.deleteById(pedido.getId());
		assertFalse(pedidoRepository.existsById(pedido.getId()));
	}

	@Test
	public void testDeleteNotFound() throws BusinessException, URISyntaxException {
		try {
			pedidoRepository.deleteById(99999L);
			assertTrue(false);
		} catch (EmptyResultDataAccessException e) {
			assertTrue(true);
		}
	}


	@Test
	public void testFindById() throws BusinessException, URISyntaxException {
		PedidoVO pedido = convertToVO(databaseLoader.createPedido(), PedidoVO.class);

		Optional<Pedido> result = pedidoRepository.findByIdFecthAll(pedido.getId());
		
		assertTrue(result.isPresent());
		assertEqualsToPedido(modelMapper.map(pedido, PedidoDTO.class), modelMapper.map(result.get(), PedidoDTO.class));
	}

	@Test
	public void testFindByCliente() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 1);

		List<PedidoVO> pedidos = convertToVO(databaseLoader.createPedidos(), PedidoVO.class);

		FiltroPedidoVO filtro = FiltroPedidoVO.builder()
				.idCliente(pedidos.get(0).getCliente().getId())
				.build();
		
		Page<Pedido> result = pedidoRepository.findByFiltro(filtro, pageable);
		for (Pedido response : result.getContent()) {
			assertEqualsToPedido(modelMapper.map(pedidos.stream().filter(a -> a.getId().equals(response.getId())).findFirst().get(), PedidoDTO.class), modelMapper.map(response, PedidoDTO.class));
		}
	}

	@Test
	public void testFindByFuncionario() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 1);

		List<PedidoVO> pedidos = convertToVO(databaseLoader.createPedidos(), PedidoVO.class);

		FiltroPedidoVO filtro = FiltroPedidoVO.builder()
				.idFuncionario(pedidos.get(0).getFuncionario().getId())
				.build();
		
		Page<Pedido> result = pedidoRepository.findByFiltro(filtro, pageable);
		for (Pedido response : result.getContent()) {
			assertEqualsToPedido(modelMapper.map(pedidos.stream().filter(a -> a.getId().equals(response.getId())).findFirst().get(), PedidoDTO.class), modelMapper.map(response, PedidoDTO.class));
		}
	}

	@Test
	public void testFindByLogradouro() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 1);

		List<PedidoVO> pedidos = convertToVO(databaseLoader.createPedidos(), PedidoVO.class);

		FiltroPedidoVO filtro = FiltroPedidoVO.builder()
				.logradouro(pedidos.get(0).getEndereco().getLogradouro())
				.build();
		
		Page<Pedido> result = pedidoRepository.findByFiltro(filtro, pageable);
		for (Pedido response : result.getContent()) {
			assertEqualsToPedido(modelMapper.map(pedidos.stream().filter(a -> a.getId().equals(response.getId())).findFirst().get(), PedidoDTO.class), modelMapper.map(response, PedidoDTO.class));
		}
	}

	@Test
	public void testFindByAllFilters() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 1);

		List<PedidoVO> pedidos = convertToVO(databaseLoader.createPedidos(), PedidoVO.class);

		FiltroPedidoVO filtro = FiltroPedidoVO.builder()
				.idCliente(pedidos.get(0).getCliente().getId())
				.idFuncionario(pedidos.get(0).getFuncionario().getId())
				.idPedido(pedidos.get(0).getId())
				.logradouro(pedidos.get(0).getEndereco().getLogradouro())
				.build();
		
		Page<Pedido> result = pedidoRepository.findByFiltro(filtro, pageable);
		for (Pedido response : result.getContent()) {
			assertEqualsToPedido(modelMapper.map(pedidos.stream().filter(a -> a.getId().equals(response.getId())).findFirst().get(), PedidoDTO.class), modelMapper.map(response, PedidoDTO.class));
		}
	}


	@Test
	public void testFindByClienteInexistente() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 1);

		List<PedidoVO> pedidos = convertToVO(databaseLoader.createPedidos(), PedidoVO.class);

		FiltroPedidoVO filtro = FiltroPedidoVO.builder()
				.idCliente(99999L)
				.build();
		
		Page<Pedido> result = pedidoRepository.findByFiltro(filtro, pageable);
		
		assertEquals(result.getContent().size(), 0);
	}

	@Test
	public void testFindByFuncionarioInexistente() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 1);

		List<PedidoVO> pedidos = convertToVO(databaseLoader.createPedidos(), PedidoVO.class);

		FiltroPedidoVO filtro = FiltroPedidoVO.builder()
				.idFuncionario(99999L)
				.build();
		
		Page<Pedido> result = pedidoRepository.findByFiltro(filtro, pageable);
		
		assertEquals(result.getContent().size(), 0);
	}
	
	@Test
	public void testFindByPedidoInexistente() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 1);

		List<PedidoVO> pedidos = convertToVO(databaseLoader.createPedidos(), PedidoVO.class);

		FiltroPedidoVO filtro = FiltroPedidoVO.builder()
				.idPedido(99999L)
				.build();
		
		Page<Pedido> result = pedidoRepository.findByFiltro(filtro, pageable);
		
		assertEquals(result.getContent().size(), 0);
	}

	@Test
	public void testFindByLogradouroInexistente() throws BusinessException, URISyntaxException {
		Pageable pageable = PageRequest.of(0, 10);

		List<PedidoVO> pedidos = convertToVO(databaseLoader.createPedidos(), PedidoVO.class);

		FiltroPedidoVO filtro = FiltroPedidoVO.builder()
				.logradouro("rua xpto")
				.build();
		
		Page<Pedido> result = pedidoRepository.findByFiltro(filtro, pageable);
		
		assertEquals(result.getContent().size(), 0);
	}

	
	@Test
	public void testCreatePedidoClienteInexistente() throws BusinessException, URISyntaxException {
		PedidoVO pedido = convertToVO(databaseLoader.builderPedido(), PedidoVO.class);
		
		Pedido pedidoToInsert = new Pedido();
		modelMapper.map(pedido, pedidoToInsert);
		
		pedidoToInsert.getCliente().setId(99999L);
		
		try {
			pedidoService.create(pedidoToInsert);
			
			assertTrue(false);
		} catch (BusinessException e) {
			assertEquals(e.getMessage(), MessagesConstants.CLIENTE_INEXISTENTE);
		}
	}

	@Test
	public void testCreatePedidoCupom() throws BusinessException, URISyntaxException {
		PedidoVO pedido = convertToVO(databaseLoader.builderPedido(), PedidoVO.class);
		
		Pedido pedidoToInsert = new Pedido();
		modelMapper.map(pedido, pedidoToInsert);
		
		pedidoToInsert.setCupom(databaseLoader.createCupom());
		
		try {
			pedidoService.create(pedidoToInsert);
			
			assertTrue(true);
		} catch (BusinessException e) {
			assertTrue(false);
		}
	}

	@Test
	public void testCreatePedidoFuncionarioInexistente() throws BusinessException, URISyntaxException {
		PedidoVO pedido = convertToVO(databaseLoader.builderPedido(), PedidoVO.class);
		
		Pedido pedidoToInsert = new Pedido();
		modelMapper.map(pedido, pedidoToInsert);
		
		pedidoToInsert.getFuncionario().setId(99999L);
		
		try {
			pedidoService.create(pedidoToInsert);
			
			assertTrue(false);
		} catch (BusinessException e) {
			assertEquals(e.getMessage(), MessagesConstants.FUNCIONARIO_INEXISTENTE);
		}
	}

	@Test
	public void testCreatePedidoEnderecoInexistente() throws BusinessException, URISyntaxException {
		PedidoVO pedido = convertToVO(databaseLoader.builderPedido(), PedidoVO.class);
		
		Pedido pedidoToInsert = new Pedido();
		modelMapper.map(pedido, pedidoToInsert);
		
		pedidoToInsert.getEndereco().setId(99999L);
		
		try {
			pedidoService.create(pedidoToInsert);
			
			assertTrue(false);
		} catch (BusinessException e) {
			assertEquals(e.getMessage(), MessagesConstants.ENDERECO_INEXISTENTE);
		}
	}

	@Test
	public void testUpdatePedidoClienteInexistente() throws BusinessException, URISyntaxException {
		PedidoVO pedido = convertToVO(databaseLoader.createPedido(), PedidoVO.class);
		
		Pedido pedidoToUpdate = new Pedido();
		modelMapper.map(pedido, pedidoToUpdate);
		
		pedidoToUpdate.getCliente().setId(99999L);
		
		try {
			pedidoService.update(pedidoToUpdate);
			
			assertTrue(false);
		} catch (BusinessException e) {
			assertEquals(e.getMessage(), MessagesConstants.CLIENTE_INEXISTENTE);
		}
	}


	@Test
	public void testUpdatePedidoFuncionarioInexistente() throws BusinessException, URISyntaxException {
		PedidoVO pedido = convertToVO(databaseLoader.createPedido(), PedidoVO.class);
		
		Pedido pedidoToUpdate = new Pedido();
		modelMapper.map(pedido, pedidoToUpdate);
		
		pedidoToUpdate.getFuncionario().setId(99999L);
		
		try {
			pedidoService.update(pedidoToUpdate);
			
			assertTrue(false);
		} catch (BusinessException e) {
			assertEquals(e.getMessage(), MessagesConstants.FUNCIONARIO_INEXISTENTE);
		}
	}

	@Test
	public void testUpdatePedidoEnderecoInexistente() throws BusinessException, URISyntaxException {
		PedidoVO pedido = convertToVO(databaseLoader.createPedido(), PedidoVO.class);
		
		Pedido pedidoToUpdate = new Pedido();
		modelMapper.map(pedido, pedidoToUpdate);
		
		pedidoToUpdate.getEndereco().setId(99999L);
		
		try {
			pedidoService.update(pedidoToUpdate);
			
			assertTrue(false);
		} catch (BusinessException e) {
			assertEquals(e.getMessage(), MessagesConstants.ENDERECO_INEXISTENTE);
		}
	}


	void assertEqualsToPedido(final PedidoDTO expected, final PedidoDTO actual) {
		assertEquals(expected.getId(), actual.getId());
		
		if(expected.getCliente()!=null && actual.getCliente()!=null){
			assertEquals(expected.getCliente().getId(), actual.getCliente().getId());
			assertEquals(expected.getCliente().getTelefone(), actual.getCliente().getTelefone());
		}

		if(expected.getFuncionario()!=null && actual.getFuncionario()!=null){
			assertEquals(expected.getFuncionario().getId(), actual.getFuncionario().getId());
			assertEquals(expected.getFuncionario().getNome(), actual.getFuncionario().getNome());
			assertEquals(expected.getFuncionario().getEmail(), actual.getFuncionario().getEmail());
		}
	
		if(expected.getEndereco()!=null && actual.getEndereco()!=null){
			assertEquals(expected.getEndereco().getId(), actual.getEndereco().getId());
		}
		
		assertEquals(expected.getDataEntrega(), actual.getDataEntrega());
		assertEquals(expected.getDataEnvio(), actual.getDataEnvio());
		assertEquals(expected.getDataPedido(), actual.getDataPedido());
		assertEquals(expected.getFrete(), actual.getFrete());
		assertEquals(expected.getNomeDestinatario(), actual.getNomeDestinatario());
	}

}
